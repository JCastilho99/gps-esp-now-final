#include <Arduino.h>
#include <ESP8266WiFi.h>
extern "C"
{
#include "user_interface.h" //biblioteca para ler e escrever na RTC
#include <espnow.h>         //biblioteca esp_now
}
#include "Ubx_Gps.cpp"

//Adafruit LIS3DH
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_LIS3DH_1.h>
#include <Adafruit_Sensor.h>

//DEFINES
#define SLEEPTIME 5e6   //tempo de sleep normal
#define SLEEPTIME2 10e6 //tempo de sleep parado
#define GET 987766      //codigo para ver se o boot é para ler o sensor
#define SEND 748934     //codigo para ver se o boot é para ler os valores da RTC
#define RTCMEMSTART 66  //endereço onde vamos começar a escrever as structs 64+2. No endereço 64 e 65 vai ficar a rtcManagement
#define MAXREADINGS 6   //numero de structs que vai mandar de cada vez
#define CHANNEL 6       //channel para o esp_now, tem que ser igual ao channel do WiFi

int blocos;                                                                                                         //numero de pacotes de 4 bits que vai ocupar a LIS3DH_DATA
uint8_t macMaster[] = {0xCC, 0x50, 0xE3, 0xC7, 0x67, 0x80};                                                         //MAC address do esp8266 "master"
uint8_t macSlave[] = {0x3E, 0x71, 0xBF, 0x2A, 0xD9, 0x23};                                                          //MAC address do esp8266 "slave"
uint8_t kok[16] = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66}; //chave para a chave
uint8_t key[16] = {0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44}; //chave para a comunicação esp_now

typedef struct
{                   //struct que ajuda na leitura da rtc
  int codigo;       //codigo de boot
  int valueCounter; //numero de dados escritos na rtc
} rtcManagementStruct;

rtcManagementStruct rtcManagement;

struct GPS_DATA
{ //struct com os dados do gps
  long time;
  long lat;
  long lon;
  unsigned char numSV;
  unsigned char gpsFix;
} rtcMem;

GPS gps;

//Pin do interrupt do acc
int intPin = D5;
//Declaração do objecto acc
Adafruit_LIS3DH acc = Adafruit_LIS3DH();

//FUNÇÕES
void print_data(GPS_DATA *obj);                                  //DEBUG
void initWiFi();                                                 //Inicializa o WiFi
void initEspNow();                                               //Inicializa o protocolo ESP_NOW
void sendReading(GPS_DATA);                                      //Envia as leituras por ESP_NOW
GPS_DATA getReading(GPS gps, bool &read_posllh, bool &read_sol); //Lê o gps
void configGPS(GPS gps);                                         //Configura o GPS
void initLIS3DH();                                               //Inicializa o acc

void setup()
{
  pinMode(D0, WAKEUP_PULLUP); //Para o esp acordar do DEEP_SLEEP
  Serial.begin(9600);

  //Init do acc
  initLIS3DH();
  //Modo de deteção de descanço
  acc.setRestDetection();

  //Calculo do numero de blocos (4 bits) de GPS_DATA
  blocos = (sizeof(rtcMem) / 4);

  system_rtc_mem_read(64, &rtcManagement, sizeof(rtcManagement));

  //DEBUG
  Serial.print("Booting in mode: ");
  if (rtcManagement.codigo == GET)
  {
    Serial.println("GET");
  }
  else if (rtcManagement.codigo == SEND)
  {
    Serial.println("SEND");
  }
  else
  {
    Serial.println("INITIAL VALUES NOT SET");
  }

  //Nenhum dos modos detetados... Prepara GET boot
  if (rtcManagement.codigo != GET && rtcManagement.codigo != SEND)
  {
    rtcManagement.codigo = GET;
    rtcManagement.valueCounter = 0;
    system_rtc_mem_write(64, &rtcManagement, sizeof(rtcManagement));
    Serial.println("Initial values set! Booting with code GET...");
    //Sleep curto para dar logo reset
    ESP.deepSleep(10, WAKE_NO_RFCAL);
  }

  //Se o codigo for GET temos de ler o sensor
  if (rtcManagement.codigo == GET)
  {
    gps.configGPS(0);

    //Variaveis para verificar se o esp já recebeu as mensagens POSLLH e SOL do gps
    bool read_posllh = false;
    bool read_sol = false;

    //Se o numero de valores for menor que 6
    if (rtcManagement.valueCounter < MAXREADINGS)
    {
      Serial.println("Reading the GPS...");
      //Enquanto não receber as leituras
      while (read_posllh == false || read_sol == false)
      {
        rtcMem = getReading(gps, read_posllh, read_sol);
      }
      Serial.println("Success!");
      print_data(&rtcMem);                                            //DEBUG
      int rtcPos = RTCMEMSTART + rtcManagement.valueCounter * blocos; //Posição onde vai escrever na RTC aumenta cada vez que um valor novo é escrito (valueCounter)
      system_rtc_mem_write(rtcPos, &rtcMem, sizeof(rtcMem));
      Serial.printf("Value written in RTC in %d \n", rtcPos); //DEBUG
      rtcManagement.valueCounter++;

      //Atualizar a rtcManagment
      system_rtc_mem_write(64, &rtcManagement, sizeof(rtcManagement));

      ESP.wdtDisable(); // Disables soft wdt reset... watchdog timer para o ESP não crashar devido a estar "preso no for..."
      // Lê o acc durante 5 segundos para ver se há movimento
      Serial.println("Checking movement...");
      for (uint32_t tStart = millis(); (millis() - tStart) < 5000;)
        ;
      //Se o interrupt pin estiver HIGH quer dizer que o acc está parado à 7 segundos
      if (digitalRead(intPin) == 1)
      {
        Serial.println("ACC parado... Going to sleep for 10 seconds");
        ESP.deepSleep(SLEEPTIME2, WAKE_NO_RFCAL);
      }
      else
      {
        Serial.println("Movimento detectado...GET routine complete! Going to sleep for 5 seconds");
        ESP.deepSleep(SLEEPTIME, WAKE_NO_RFCAL); //NO_RFCAL serve para não fazer a calibração do WiFi
      }
    }
    else
    { //Se o numero de valores escritos na RTC já for maior que 6 então temos de preparar a rotina de SEND
      rtcManagement.codigo = SEND;
      rtcManagement.valueCounter = 0;
      system_rtc_mem_write(64, &rtcManagement, sizeof(rtcManagement));
      Serial.println("Booting in SEND mode...");
      ESP.deepSleep(10, WAKE_RFCAL); //WAKE_RFCAL serve para quando der boot vai preparar o WiFi
    }
  }
  else
  { //Se o codigo for SEND
    //Liga o WiFi e o esp_now
    initWiFi();
    initEspNow();

    Serial.println("Sending values!");
    for (int i = 0; i < MAXREADINGS; i++)
    {
      int rtcPos = RTCMEMSTART + i * blocos;
      system_rtc_mem_read(rtcPos, &rtcMem, sizeof(rtcMem));
      sendReading(rtcMem);
    }
    Serial.println("SEND routine complete!");

    //Depois de ler os valores todos temos de preparar o boot em modo GET
    rtcManagement.codigo = GET;
    rtcManagement.valueCounter = 0;
    system_rtc_mem_write(64, &rtcManagement, sizeof(rtcManagement));
    Serial.print("Booting in GET mode");
    ESP.deepSleep(10, WAKE_NO_RFCAL);
  }
}

void loop() {} // No loop não precisa de fazer nada porque funciona com os deepSleeps

void initEspNow()
{
  if (esp_now_init() != 0)
  {
    Serial.println("*** ESP_now init failed ***");
    ESP.restart();
  }
  delay(1);

  esp_now_set_self_role(ESP_NOW_ROLE_CONTROLLER);

  //Configuração da encryptação das mensagens
  esp_now_set_kok(kok, 16);

  esp_now_add_peer(macSlave, ESP_NOW_ROLE_SLAVE, CHANNEL, key, 16); //adds slave to peer list
  esp_now_set_peer_key(macSlave, key, 16);

  esp_now_register_send_cb([](uint8_t *mac, uint8_t sendStatus) {    //callback para saber se a struct foi enviada
    Serial.printf("send_cb, send done, status = %i \n", sendStatus); //sendStatus =0 --> Sucesso / sendStatus =1 -->Falhou
  });
}

void initWiFi()
{
  WiFi.mode(WIFI_STA);
  WiFi.begin();
  Serial.println();
  Serial.print("Mac Address in Station: ");
  Serial.println(WiFi.macAddress().c_str());
}

void initLIS3DH()
{
  Serial.println("LIS3DH starting!");

  if (!acc.begin(0x18))
  { // change this to 0x19 for alternative i2c address
    Serial.println("Couldnt start");
    while (1)
      ;
  }
  Serial.println("LIS3DH found!");
}

void sendReading(GPS_DATA sd)
{
  u8 bs[sizeof(sd)];
  memcpy(bs, &sd, sizeof(sd));
  esp_now_send(NULL, bs, sizeof(bs));
  delay(20);
}

GPS_DATA getReading(GPS gps, bool &read_posllh, bool &read_sol)
{
  struct GPS_DATA *newdata = (struct GPS_DATA *)malloc(sizeof(struct GPS_DATA));
  int msgType = gps.processGPS();
  if (msgType == MT_NAV_POSLLH)
  {
    //Guarda as novas leituras da POSLLH na variavel global rtcMem
    newdata->lat = ubxMessage.navPosllh.lat;
    newdata->lon = ubxMessage.navPosllh.lon;
    newdata->time = ubxMessage.navPosllh.iTOW;
    //Já leu a mensagem de NAV_POSLLH
    read_posllh = true;
  }
  else if (msgType == MT_NAV_SOL)
  {
    //Guarda as novas leituras da SOL na variavel global rtcMem
    newdata->numSV = ubxMessage.navSol.numSV;
    newdata->gpsFix = ubxMessage.navSol.gpsFix;
    //Já leu a mensagem de NAV_SOL
    read_sol = true;
  }
  else
  {
    delay(10); //Sem este delay o programa não funciona....?
  }
  return *newdata;
}

void print_data(GPS_DATA *obj)
{
  Serial.print("ITOW: ");
  Serial.println(obj->time);
  Serial.print("lat/lon: ");
  Serial.print(obj->lat / 1000000.0f);
  Serial.print("/");
  Serial.println(obj->lon / 1000000.0f);
  Serial.print("numSVs: ");
  Serial.println(obj->numSV);
  Serial.print("gpsFix: ");
  Serial.println(obj->gpsFix);
}
