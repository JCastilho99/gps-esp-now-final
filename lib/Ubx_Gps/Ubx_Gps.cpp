#include "Ubx_Gps.h"
#include <Arduino.h>

//Envia mensagem de configuração para o GPS
void GPS::sendUBXCommand(const unsigned char *message, int size)
{
  for (int i = 0; i < size; i++)
  {
    Serial.write(pgm_read_byte(message + i));
  }
}

//Calcula os parametros de checksum para ver se a mensagem é valida
void calcChecksum(unsigned char *CK, int msgSize)
{
  memset(CK, 0, 2);
  for (int i = 0; i < msgSize; i++)
  {
    CK[0] += ((unsigned char *)(&ubxMessage))[i];
    CK[1] += CK[0];
  }
}

// Compara o header da mensagem para a identificar
bool compareMsgHeader(const unsigned char *msgHeader)
{
  unsigned char *ptr = (unsigned char *)(&ubxMessage);
  return ptr[0] == msgHeader[0] && ptr[1] == msgHeader[1];
}

//Parser for the Ublox messages
int GPS::processGPS()
{
  static int fpos = 0;
  static unsigned char checksum[2];

  static byte currentMsgType = MT_NONE;
  static int payloadSize = sizeof(UBXMessage);
  while (Serial.available())
  {
    byte c = Serial.read();

    if (fpos < 2)
    {
      // For the first two bytes we are simply looking for a match with the UBX header bytes (0xB5, 0x62)
      if (c == UBX_HEADER[fpos])
        fpos++;
      else
        fpos = 0; // Reset to beginning state.
    }
    else
    {
      // If we come here then fpos >= 2, which means we have found a match with the UBX_HEADER
      // and we are now reading in the bytes that make up the payload.

      // Place the incoming byte into the ubxMessage struct. The position is fpos-2 because
      // the struct does not include the initial two-byte header (UBX_HEADER).
      if ((fpos - 2) < payloadSize)
        ((unsigned char *)(&ubxMessage))[fpos - 2] = c;

      fpos++;

      if (fpos == 4)
      {
        // We have just received the second byte of the message type header,
        // so now we can check to see what kind of message it is.
        if (compareMsgHeader(NAV_POSLLH_HEADER))
        {
          currentMsgType = MT_NAV_POSLLH;
          payloadSize = sizeof(NAV_POSLLH);
        }
        else if (compareMsgHeader(NAV_STATUS_HEADER))
        {
          currentMsgType = MT_NAV_STATUS;
          payloadSize = sizeof(NAV_STATUS);
        }
        else if (compareMsgHeader(NAV_SOL_HEADER))
        {
          currentMsgType = MT_NAV_SOL;
          payloadSize = sizeof(NAV_SOL);
        }
        else
        {
          // unknown message type, bail
          fpos = 0;
        }
      }

      if (fpos == (payloadSize + 2))
      {
        // All payload bytes have now been received, so we can calculate the
        // expected checksum value to compare with the next two incoming bytes.
        calcChecksum(checksum, payloadSize);
      }
      else if (fpos == (payloadSize + 3))
      {
        // First byte after the payload, ie. first byte of the checksum.
        // Does it match the first byte of the checksum we calculated?
        if (c != checksum[0])
        {
          // Checksum doesn't match, reset to beginning state and try again.
          fpos = 0;
        }
      }
      else if (fpos == (payloadSize + 4))
      {
        // Second byte after the payload, ie. second byte of the checksum.
        // Does it match the second byte of the checksum we calculated?
        fpos = 0; // We will reset the state regardless of whether the checksum matches.
        if (c == checksum[1])
        {
          // Checksum matches, we have a valid message.
          return currentMsgType;
        }
      }
      else if (fpos > (payloadSize + 4))
      {
        // We have now read more bytes than both the expected payload and checksum
        // together, so something went wrong. Reset to beginning state and try again.
        fpos = 0;
      }
    }
  }
  // Se não der nenhuma mensagem...
  return MT_NONE;
}

// Calcula a mensagem UBX_ACK para saber se o comando UBX_CFG foi recebido corretamente
boolean GPS::getUBX_ACK(const unsigned char *MSG)
{
  uint8_t b;
  uint8_t ackByteID = 0;
  uint8_t ackPacket[10];
  unsigned long startTime = millis();

  // Construct the expected ACK packet
  ackPacket[0] = 0xB5; // header
  ackPacket[1] = 0x62; // header
  ackPacket[2] = 0x05; // class
  ackPacket[3] = 0x01; // id
  ackPacket[4] = 0x02; // length
  ackPacket[5] = 0x00;
  ackPacket[6] = pgm_read_byte(MSG + 2); // ACK class
  ackPacket[7] = pgm_read_byte(MSG + 3); // ACK id
  ackPacket[8] = 0;                      // CkS
  ackPacket[9] = 0;                      // CkS

  // Calculate the checksums
  for (uint8_t i = 2; i < 8; i++)
  {
    ackPacket[8] = ackPacket[8] + ackPacket[i];
    ackPacket[9] = ackPacket[9] + ackPacket[8];
  }

  while (1)
  {
    // Test for success
    if (ackByteID > 9)
    {
      // All packets in order!
      Serial.println(" SUCCESS!");
      return true;
    }

    // Timeout if no valid response in 5 seconds
    if (millis() - startTime > 5000)
    {
      Serial.println(" FAILED!");
      return false;
    }

    // Make sure data is available to read
    if (Serial.available())
    {
      b = Serial.read();

      // Check that bytes arrive in sequence as per expected ACK packet
      if (b == ackPacket[ackByteID])
      {
        ackByteID++;
      }
      else
      {
        ackByteID = 0; // Reset and look again, invalid order
      }
    }
  }
}

/**
 * 0 - normal mode
 * 1 - cyclic tracking 2 minutos
 * 2 - cyclic tracking 1 minuto
 * 3 - ON/OFF mode*/
void GPS::configGPS(int config)
{
  //Resets all setings to default
  do
  {
    sendUBXCommand(ResetSettings, sizeof(ResetSettings));
  } while (!getUBX_ACK(ResetSettings));

  //Disable all NMEA messages
  sendUBXCommand(DisableAllNMEA, sizeof(DisableAllNMEA));

  //Sets dynamic model to pedestrian
  do
  {
    sendUBXCommand(NAV5_Pedestrian, sizeof(NAV5_Pedestrian));
  } while (!getUBX_ACK(NAV5_Pedestrian));

  //Enable UBX_NAV_POSLLH
  do
  {
    sendUBXCommand(EnableNAV_POSLLH, sizeof(EnableNAV_POSLLH));
  } while (!getUBX_ACK(EnableNAV_POSLLH));

  //Enable UBX_NAV_SOL
  do
  {
    sendUBXCommand(EnableNAV_SOL, sizeof(EnableNAV_SOL));
  } while (!getUBX_ACK(EnableNAV_SOL));

  // Different power modes
  switch (config)
  {
  case 1:
    do
    {
      sendUBXCommand(PowerModeConfig, sizeof(PowerModeConfig));
    } while (!getUBX_ACK(PowerModeConfig));
    break;
  case 2:
    do
    {
      sendUBXCommand(PowerModeConfig2, sizeof(PowerModeConfig2));
    } while (!getUBX_ACK(PowerModeConfig2));
    break;
  case 3:
    do
    {
      sendUBXCommand(PowerModeConfig3, sizeof(PowerModeConfig3));
    } while (!getUBX_ACK(PowerModeConfig3));
    break;
  default:
    break;
  }

  //Change the message rate
  do
  {
    sendUBXCommand(Ubx1Hz, sizeof(Ubx1Hz));
  } while (!getUBX_ACK(Ubx1Hz));

  //Saves configuration
  do
  {
    sendUBXCommand(SaveConfig, sizeof(SaveConfig));
  } while (!getUBX_ACK(SaveConfig));
}

double deg2rad(double deg)
{
  return deg * (PI / 180);
}

// Calcula a distancia entre dois pontos
long GPS::distanceBetween(geoPoint a, geoPoint b)
{
  double R = 6371;
  double Lat = deg2rad(b.lat - a.lat);
  double Lon = deg2rad(b.lon - a.lon);

  // Haversine formula https://en.wikipedia.org/wiki/Haversine_formula
  double c = (sin(Lat / 2) * sin(Lat / 2)) + cos(deg2rad(a.lat)) * cos(deg2rad(b.lat)) * (sin(Lon / 2) * sin(Lon / 2));
  double angle = 2 * atan2(sqrt(c), sqrt(1 - c));

  return angle * R;
}

bool GPS::checkGeofence(geoPoint a, geoPoint b)
{
  //true: inside
  //false: outside
  return (distanceBetween(a, b) < RADIUS);
}